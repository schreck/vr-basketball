using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dribbeln : MonoBehaviour
{
    public float DribbelStaerke = 1.15f;

    private SG.SG_Interactable isItGrabbed;
    private bool isGrabbed = false;

    private void OnCollisionStay(Collision collision)
    {
        if (!isItGrabbed.IsGrabbed())
        {
            isGrabbed = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {

        if (!isGrabbed)
        {
            if (collision.collider.tag == "Ball")
            {
                collision.rigidbody.velocity *= DribbelStaerke;
            }
        }
        isGrabbed = false;
    }
}
