using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpawnBall : MonoBehaviour
{
	public SG.SG_TrackedHand Hand;

	// gameobject to transform when gesture made
	[SerializeField] private GameObject ballPrefab;

	// finger presets for thumbsUp gesture
	public static float[] thumbsUpThresholds = new float[5] { 0.2f, 0.7f, 0.7f, 0.7f, 0.7f };
	public static float thumbPassThreshold = 0.1f;

	protected bool[] confirmGest = new bool[5];


	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		/*
        if (Input.GetKeyDown(KeyCode.L))
        {
			Spawn();
        }*/

		if(CheckConfirm(true, Hand, ref this.confirmGest))
        {
			print("Daumen hoh check.");
			Spawn();
        }

    }

	public static bool CheckConfirm(bool shouldCheck, SG.SG_TrackedHand hand, ref bool[] gest)
	{
		if (shouldCheck)
		{
			float[] normalizedFlexion;
			if (hand != null && hand.GetNormalizedFlexion(out normalizedFlexion))
			{
				bool allGood = true;
				//Debug.Log(SG.Util.SG_Util.ToString(normalizedFlexion));
				for (int f = 0; f < normalizedFlexion.Length && f < gest.Length && f < thumbsUpThresholds.Length; f++)
				{
					if (gest[f]) //finger is currently making a gesture
					{
						if (f == 0) { gest[f] = normalizedFlexion[f] < thumbsUpThresholds[f] + thumbPassThreshold; }
						else { gest[f] = normalizedFlexion[f] > thumbsUpThresholds[f] - thumbPassThreshold; }
					}
					else //finger is not yet in the right spot
					{
						if (f == 0) { gest[f] = normalizedFlexion[f] < thumbsUpThresholds[f]; }
						else { gest[f] = normalizedFlexion[f] > thumbsUpThresholds[f]; }
					}
					if (!gest[f]) { allGood = false; }
				}
				return allGood;
			}
			return false;
		}
		return true;
	}

	private void Spawn()
    {

		ballPrefab.transform.position = transform.position + 0.8f * Vector3.up;
		ballPrefab.transform.rotation = Quaternion.identity;
		ballPrefab.GetComponent<Rigidbody>().velocity = Vector3.zero;

	}
}
