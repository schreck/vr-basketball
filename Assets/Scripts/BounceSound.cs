using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceSound : MonoBehaviour
{
    
    // Audio Library
    public AudioClip[] softdribble; // soft
    public AudioClip[] harddribble; // hard

    // define source object
    private AudioSource source; // LEGACY: = gameObject.AddComponent<AudioSource>();

    // define pitch variation
    private float lowPitchRange = 0.8F;
    private float highPitchRange = 1.2F;

    // define velocity to volume modification
    private float velToVol = 0.05F;
    private float velClipSplit = 1.0F;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();   // shorten term
        source.playOnAwake = false;             // disable play on activation
        // source.clip = dribble;
        
    }
    
    // Update is called once per frame
    void Update()
    {

    }

    // Plays sound when collision detected
    void OnCollisionEnter(Collision coll)
    {
        source.Play();
        GetComponent<AudioSource>().Play();
        // Pick a random pitch
        source.pitch = Random.Range(lowPitchRange, highPitchRange);
        // adjust pitch to velocity
        float hitVol = coll.relativeVelocity.magnitude * velToVol;
        //print("Volume = " + hitVol);

        // Debug
        if (softdribble.Length == 0) { Debug.Log("Method \"OnCollisionEnter\" requires at least one argument for softClips!"); return; }
        if (harddribble.Length == 0) { Debug.Log("Method \"OnCollisionEnter\" requires at least one argument fpr hardClips!"); return; }

        // choose between soft and hard clips
        if (coll.relativeVelocity.magnitude < velClipSplit)
        {
            source.clip = softdribble[Random.Range(0, softdribble.Length)];
            source.PlayOneShot(source.clip, hitVol);
        }
        else
        {
            source.clip = harddribble[Random.Range(0, harddribble.Length)];
            source.PlayOneShot(source.clip, hitVol);
        }

    }
}
