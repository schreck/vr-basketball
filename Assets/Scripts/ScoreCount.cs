using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCount : MonoBehaviour
{
    [Header("Current Score")]
    [Range(0,99)] public int score;

    [Header("Scoreboard Game Objects")]
    public Renderer board0;
    public Renderer board1;

    [Header("Material Sources")]
    public Material zero;
    public Material one;
    public Material two;
    public Material three;
    public Material four;
    public Material five;
    public Material six;
    public Material seven;
    public Material eight;
    public Material nine;

    // Start is called before the first frame update
    void Start()
    {
        //board0 = GetComponentInChildren<Renderer>(); 
        //board1 = GetComponentInChildren<Renderer>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collider)
    {
        score++;
        print (score);
        switch (score%10)
        {
            case 0:
                board1.material = zero;
                break;
            case 1:
                board1.material = one;
                break;
            case 2:
                board1.material = two;
                break;
            case 3:
                board1.material = three;
                break;
            case 4:
                board1.material = four;
                break;
            case 5:
                board1.material = five;
                break;
            case 6:
                board1.material = six;
                break;
            case 7:
                board1.material = seven;
                break;
            case 8:
                board1.material = eight;
                break;
            case 9:
                board1.material = nine;
                break;
        }
        switch (score/10)
        {
            case 0:
                board0.material = zero;
                break;
            case 1:
                board0.material = one;
                break;
            case 2:
                board0.material = two;
                break;
            case 3:
                board0.material = three;
                break;
            case 4:
                board0.material = four;
                break;
            case 5:
                board0.material = five;
                break;
            case 6:
                board0.material = six;
                break;
            case 7:
                board0.material = seven;
                break;
            case 8:
                board0.material = eight;
                break;
            case 9:
                board0.material = nine;
                break;
        }
    }
}
