using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JumpToScene : MonoBehaviour
{
    public SG.SG_TrackedHand Hand;

    // finger presets for thumbsUp gesture
    private static float[] peaceGesture = new float[5] { 0.7f, 0.1f, 0.1f, 0.7f, 0.7f };
    public static float thumbPassThreshold = 0.1f;

    protected bool[] checkGest = new bool[5];

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckConfirm(true, Hand, ref this.checkGest))
        {
            ChangeScene();
        }
        
    }

    public static bool CheckConfirm(bool shouldCheck, SG.SG_TrackedHand hand, ref bool[] gest)
    {
        if (shouldCheck)
        {
            float[] normalizedFlexion;
            if (hand != null && hand.GetNormalizedFlexion(out normalizedFlexion))
            {
                bool allGood = true;
                //Debug.Log(SG.Util.SG_Util.ToString(normalizedFlexion));
                for (int f = 0; f < normalizedFlexion.Length && f < gest.Length && f < peaceGesture.Length; f++)
                {
                    if (gest[f]) //finger is currently making a gesture
                    {
                        if (f == 0) { gest[f] = normalizedFlexion[f] < peaceGesture[f] + thumbPassThreshold; }
                        else { gest[f] = normalizedFlexion[f] > peaceGesture[f] - thumbPassThreshold; }
                    }
                    else //finger is not yet in the right spot
                    {
                        if (f == 0) { gest[f] = normalizedFlexion[f] < peaceGesture[f]; }
                        else { gest[f] = normalizedFlexion[f] > peaceGesture[f]; }
                    }
                    if (!gest[f]) { allGood = false; }
                }
                return allGood;
            }
            return false;
        }
        return true;
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene("CalibrationVoid_XR");
    }
}
