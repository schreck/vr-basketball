Dieses Projekt ist für die Verwendung von Senseglove Nova Handschuhen in Kombination mit einem VR Rig entwickelt und braucht diese um gespielt zu werden.
Im VR Labor des Instituts Didaktik der Informatik an der HU Berlin (Adlershof) können Nova Handschuhe und VR Rig ausprobiert werden. Für den Zugang zu den Handschuhen, müsst ihr (Stand 06.09.2023) Stephan Schreck, oder Frank Wehrmann kontaktieren.

Lest bitte die Dokumentation bevor Ihr die Applikation startet!

Für die Weiterarbeit mit diesem Projekt, achtet auf die verwendeten Lizenzen der verwendeten Assets.
